Feature: Trigger the API on the basis of given input data

@Datadriven
Scenario Outline: Trigger the API request with valid request parameters
          Given "<Name>" and "<Job>" in request body
          When send the request with given data payload to the endpoint
          Then validate status code for Datadriven testcase 
          And validate response body parameter for Datadriven testcase

Examples:
          |Name |Job |
          |Vicky|Devloper|
          |Harry|Tester|
          
          

