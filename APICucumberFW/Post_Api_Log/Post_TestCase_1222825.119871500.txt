Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "Harry",
    "job": "Tester"
}

Response header date is : 
Sun, 17 Mar 2024 16:57:29 GMT

Response body is : 
{"name":"Harry","job":"Tester","id":"460","createdAt":"2024-03-17T16:57:29.866Z"}