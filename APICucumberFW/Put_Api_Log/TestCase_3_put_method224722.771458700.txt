Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "zion resident"
}

Response header date is : 
Fri, 15 Mar 2024 17:16:28 GMT

Response body is : 
{"name":"morpheus","job":"zion resident","updatedAt":"2024-03-15T17:16:28.630Z"}