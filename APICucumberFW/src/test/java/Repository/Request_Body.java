package Repository;

import java.io.IOException;
import java.util.ArrayList;

import org.testng.annotations.DataProvider;

import Common_Methods.Utility;

public class Request_Body extends Environment {

	public static String Req_Tc1() throws IOException {
		ArrayList<String> Data = Utility.readExceldata("Post_Api" ,"TC_1");
		System.out.println(Data);
		String Key_name = Data.get(1);
		String Value_name = Data.get(2);
		String Key_job = Data.get(3);
		String Value_job = Data.get(4);

		String req_body = "{\r\n" + "    \" " + Key_name + "\": \"" + Value_name + "\",\r\n" + "    \"" + Key_job
				+ "\": \"" + Value_job + "\"\r\n" + "}";
		return req_body;
	}

	public static String Req_Tc2() throws IOException {
		ArrayList<String> Data = Utility.readExceldata("Post_Api", "TC_2");
		System.out.println(Data);
		String Key_name = Data.get(1);
		String Value_name = Data.get(2);
		String Key_job = Data.get(3);
		String Value_job = Data.get(4);
		String req_body = "{\r\n" + "    \"" + Key_name + "\": \"" + Value_name + "\",\r\n" + "    \"" + Key_job
				+ "\": \"" + Value_job + "\"\r\n" + "}";
		return req_body;
	}

	public static String Req_Put_TestCase() {
		String req_body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		return req_body;
	}

	public static String Req_Patch_TestCase() {
		String req_body = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		return req_body;
	}

	public static String Req_Get_TestCase() {
		String req_body = "";
		return req_body;
	}
	
	@DataProvider()
	public Object[][] requestBody(){
		return new Object[][]
				{
			         {"morpheus","leader"},
			         {"Vinayak","Qa"}
				};

}}
