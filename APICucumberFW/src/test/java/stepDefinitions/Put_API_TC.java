package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.AssertJUnit;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Request_Body;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Put_API_TC {

	String requestbody;
	String Endpoint;
	Response response;
	String res_body;
	int statuscode;
	String res_name;
	String res_job;
	String res_updatedAt;

	@Given("name and job in Put request body")
	public void name_and_job_in_put_request_body() throws IOException {
		RestAssured.config = RestAssuredConfig.config().sslConfig(SSLConfig.sslConfig().relaxedHTTPSValidation());

		File dir_name = Utility.CreateLogDirectory("Put_Api_Log");

		requestbody = Request_Body.Req_Put_TestCase();

		Endpoint = Request_Body.Hostname() + Request_Body.Resource_Put_TestCase();

		response = API_Trigger.PUT_Trigger(Request_Body.Headername(), Request_Body.Headervalue(),
				Request_Body.Req_Put_TestCase(), Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("TestCase_3_put_method"), dir_name, Endpoint,
				Request_Body.Req_Put_TestCase(), response.getHeader("Date"), response.getBody().asString());

	}

	@When("send the Put request with payload to the endpoint")
	public void send_the_put_request_with_payload_to_the_endpoint() {
		res_body = (response.getBody().asString());
		statuscode = (response.statusCode());
		res_name = (response.jsonPath().getString("name"));
		res_job = (response.jsonPath().getString("job"));
		res_updatedAt = response.jsonPath().getString("updatedAt").substring(0, 11);

	}

	@Then("validate status code of Put API")
	public void validate_status_code_of_put_api() {
		Assert.assertEquals(statuscode, 200);
	}

	@Then("validate response body parameters of Put API")
	public void validate_response_body_parameters_of_put_api() {
		JsonPath jsp_req = new JsonPath(Request_Body.Req_Put_TestCase());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		AssertJUnit.assertEquals(response.jsonPath().getString("name"), req_name);
		AssertJUnit.assertEquals(response.jsonPath().getString("job"), req_job);
		AssertJUnit.assertEquals(response.jsonPath().getString("updatedAt").substring(0, 11), expecteddate);
	}

}
