package Common_Methods;

import Repository.Request_Body;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API_Trigger {
	public static Response Post_trigger(String Headername, String Headervalue, String Req_Body, String Endpoint) {

		// STEP 2: BUILD THE REQUEST SPECIFICATION BY USING REQUEST SPECIFICATION CLASS

		RequestSpecification req_spec = RestAssured.given();

		// STEP 2.1: SET THE REQUEST HEADER

		req_spec.header(Headername, Headervalue);

		// STEP 2.2: SET THE REQUEST BODY

		req_spec.body(Req_Body);

		// STEP 2.3: TRIGGER THE REQUEST BODY

		Response response = req_spec.post(Endpoint);

		return response;
	}

	public static Response Patch_trigger(String Headername, String Headervalue, String Req_Body, String Endpoint) {

		RequestSpecification req_spec = RestAssured.given();

		req_spec.header(Headername, Headervalue);

		req_spec.body(Req_Body);

		Response response = req_spec.patch(Endpoint);

		return response;
	}

	public static Response PUT_Trigger(String Headername, String Headervalue, String Req_Body, String Endpoint) {

		RequestSpecification req_spec = RestAssured.given();

		req_spec.header(Headername, Headervalue);

		req_spec.body(Req_Body);

		Response response = req_spec.put(Endpoint);

		return response;
	}

	public static Response Get_Trigger(String Headername, String Headervalue, String Endpoint) {

		RequestSpecification req_spec = RestAssured.given();

		req_spec.header(Headername, Headervalue);

		Response response = req_spec.get(Endpoint);

		return response;
	}

	public static Response Delete_Trigger(String Headername, String Headervalue, String Endpoint) {

		RequestSpecification req_spec = RestAssured.given();

		req_spec.header(Headername, Headervalue);

		Response response = req_spec.delete(Endpoint);

		return response;
	}

}
