Feature: Trigger the Patch API and validate the Response body and Response parameters
@PatchAPITestcase
Scenario: Trigger the Patch api request with valid String request body parameters
          Given name and job in Patch request body
          When send the Patch request with payload to the endpoint
          Then validate status code of Patch API
          And validate response body parameters of Patch API
