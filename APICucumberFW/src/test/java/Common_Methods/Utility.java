package Common_Methods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utility {

  	public static ArrayList<String> readExceldata(String Sheetname, String Testcase) throws IOException {
		ArrayList<String> arraydata = new ArrayList<String>();

		// Step 1 : Fetch the Java project name and location

		String projectDir = System.getProperty("user.dir");
		System.out.println("Current project Directory Is :" + projectDir);

		// Step 2 : Create an object of file input stream to locate the excel file

		FileInputStream fls = new FileInputStream(projectDir + "\\Data_Files\\Input_Data.xlsx");

		// step 3: create an object to XSSFWorkbook to open the excel file

		// Step 3.1: Fetch the count sheets
		XSSFWorkbook wb = new XSSFWorkbook(fls);
		int count = wb.getNumberOfSheets();
		// System.out.println("Count of sheets is :" + count);

		// step 4: fetch the names of sheets:

		for (int i = 0; i < count; i++) {

			if (wb.getSheetName(i).equals(Sheetname)) {
				// System.out.println("sheet at index " + i + ":" + wb.getSheetName(i));

				// STEP 4.2: ACCEESS THE DATA FROM SHEET
				XSSFSheet datasheet = wb.getSheetAt(i);
				Iterator<Row> rows = datasheet.iterator();
				while (rows.hasNext()) {
					Row datarows = rows.next();

					String Tcname = datarows.getCell(0).getStringCellValue();
					if (Tcname.equals(Testcase)) {
						Iterator<Cell> cellvalues = datarows.cellIterator();
						while (cellvalues.hasNext()) {
							String testdata = cellvalues.next().getStringCellValue();
							// System.out.println(testdata);
							arraydata.add(testdata);
						}
					} else {
						System.out.println(Testcase + "Testcase not found in sheet:" + wb.getSheetName(i));
					}

				}

				break;
			} else {
				System.out.println(Sheetname + "Sheet is not found in file Input_Data.xlsx at Index:" + i);
			}
		}
		wb.close();
		return arraydata;

	}

	public static void evidenceFileCreator(String Filename, File FileLocation, String endpoint, String RequestBody,
			String ResHeader, String ResponseBody) throws IOException {
		// Step 1 : Create and Open the file
		File newTextFile = new File(FileLocation + "\\" + Filename + ".txt");
		System.out.println("File create with name: " + newTextFile.getName());

		// Step 2 : Write data
		FileWriter writedata = new FileWriter(newTextFile);
		writedata.write("Endpoint is :\n" + endpoint + "\n\n");
		writedata.write("Request body is :\n" + RequestBody + "\n\n");
		writedata.write("Response header date is : \n" + ResHeader + "\n\n");
		writedata.write("Response body is : \n" + ResponseBody);

		// Step 3 : Save and close
		writedata.close();

	}

	public static File CreateLogDirectory(String dirName) {

		// Step 1 : Fetch the Java project name and location

		String projectDir = System.getProperty("user.dir");
		System.out.println("Current project Directory Is :" + projectDir);

		// Step 2 : Verify weather the directory in variable dirName exists inside the
		// projectDir and act accordingly

		File directory = new File(projectDir + "\\" + dirName);

		if (directory.exists()) {
			System.out.println(directory + " , File Already Exists");
		} else {
			System.out.println(directory + " , File Doesnt exists , Hence creating it");
			directory.mkdir();
			System.out.println(directory + " , Created");
		}

		return directory;

	}

	public static String testLogName(String Name) {
		LocalTime currentTime = LocalTime.now();
		String currentstringTime = currentTime.toString().replaceAll(":", "");
		String TestLogName = Name + currentstringTime;
		return TestLogName;
	}

}