package Repository;

public class Environment {

	public static String Hostname() {
		String Hostname = "https://reqres.in";
		return Hostname;
	}

	public static String Resource() {
		String Resource = "/api/users";
		return Resource;
	}

	public static String Resource_Patch_TasteCase() {
		String Resource = "/api/users/2";
		return Resource;
	}

	public static String Resource_Put_TestCase() {
		String Resource = "/api/users/2";
		return Resource;
	}

	public static String Resource_Get_Testcase() {
		String Resource = "/api/users?page=2";
		return Resource;
	}

	public static String Resource_Delect_Testcase() {
		String Resource = "/api/users/2";
		return Resource;
	}

	public static String Headername() {
		String Headername = "Content-Type";
		return Headername;
	}

	public static String Headervalue() {
		String Headervalue = "application/json";
		return Headervalue;
	}
}
