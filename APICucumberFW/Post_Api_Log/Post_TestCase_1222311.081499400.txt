Endpoint is :
https://reqres.in/api/users

Request body is :
{
    " Name": "Vinayak",
    "Job": "QA"
}

Response header date is : 
Fri, 15 Mar 2024 16:52:17 GMT

Response body is : 
{" Name":"Vinayak","Job":"QA","id":"618","createdAt":"2024-03-15T16:52:16.998Z"}