package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.AssertJUnit;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Request_Body;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Patch_API_TC {

	String RequestBody;
	String Endpoint;
	Response response;
	int statuscode;
	String res_body;
	String res_name;
	String res_job;
	String res_updatedAt;

	@Given("name and job in Patch request body")
	public void name_and_job_in_patch_request_body() throws IOException {
		RestAssured.config = RestAssuredConfig.config().sslConfig(SSLConfig.sslConfig().relaxedHTTPSValidation());

		File Dir_name = Utility.CreateLogDirectory("Patch_Api_Log");
		RequestBody = Request_Body.Req_Patch_TestCase();

		Endpoint = Request_Body.Hostname() + Request_Body.Resource_Patch_TasteCase();

		response = API_Trigger.Patch_trigger(Request_Body.Headername(), Request_Body.Headervalue(),
				Request_Body.Req_Patch_TestCase(), Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("TestCase_4_Patch_method"), Dir_name, Endpoint,
				Request_Body.Req_Patch_TestCase(), response.getHeader("Date"), response.getBody().asPrettyString());

	}

	@When("send the Patch request with payload to the endpoint")
	public void send_the_patch_request_with_payload_to_the_endpoint() {
		statuscode = (response.statusCode());
		res_body = response.getBody().asString();
		res_name = response.jsonPath().getString("name");
		res_job = response.jsonPath().getString("job");
		res_updatedAt = response.getBody().jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);

	}

	@Then("validate status code of Patch API")
	public void validate_status_code_of_patch_api() {
		Assert.assertEquals(statuscode, 200);
	}

	@Then("validate response body parameters of Patch API")
	public void validate_response_body_parameters_of_patch_api() {
		JsonPath jsp_req = new JsonPath(Request_Body.Req_Patch_TestCase());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_updatedAt.substring(0, 11), expecteddate);

	}

}
