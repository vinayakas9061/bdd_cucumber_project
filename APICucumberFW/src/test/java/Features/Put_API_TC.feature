Feature: Trigger the Put API and validate the Response body and Response parameters 

@PutAPITestcase 
Scenario: Trigger the Put api request with valid String request body parameters 
	Given name and job in Put request body 
	When send the Put request with payload to the endpoint 
	Then validate status code of Put API 
	And validate response body parameters of Put API 
