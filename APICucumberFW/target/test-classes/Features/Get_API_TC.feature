Feature: Trigger the GET API and validate the Response body and Response parameters
@GetAPITestcase
Scenario: Trigger the GET api request with valid String request body parameters
          Given name and job in GET request body
          When send the GET request to the endpoint
          Then validate status code of GET API
          And validate response body parameters of GET API
          
