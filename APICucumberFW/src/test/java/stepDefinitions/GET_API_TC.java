package stepDefinitions;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Request_Body;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GET_API_TC {

	Response response;
	String res_body;
	int status_code;
	int i;

	@Given("name and job in GET request body")
	public void name_and_job_in_get_request_body() throws IOException {
		RestAssured.config = RestAssuredConfig.config().sslConfig(SSLConfig.sslConfig().relaxedHTTPSValidation());

		File Dir_Name = Utility.CreateLogDirectory("Get_Api_Log");

		String Endpoint = Request_Body.Hostname() + Request_Body.Resource_Get_Testcase();

		response = API_Trigger.Get_Trigger(Request_Body.Headername(), Request_Body.Headervalue(), Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("TestCase_5_Get_method"), Dir_Name, Endpoint,
				Request_Body.Req_Get_TestCase(), response.getHeader("Date"), response.getBody().asPrettyString());
	}

	@When("send the GET request to the endpoint")
	public void send_the_get_request_with_payload_to_the_endpoint() {
		res_body = response.getBody().asPrettyString();
		status_code = response.statusCode();
	}

	@Then("validate status code of GET API")
	public void validate_status_code_of_get_api() {
		Assert.assertEquals(status_code, 200);
	}

	@Then("validate response body parameters of GET API")
	public void validate_response_body_parameters_of_get_api() {
		JsonPath res_jsn = new JsonPath(res_body);
		int count = res_jsn.getInt("data.size()");
		System.out.println(count);
		int id = 0;
		int idArr[] = new int[count];
		int ids[] = { 7, 8, 9, 10, 11, 12 };
		for (int i = 0; i < count; i++) {

			id = res_jsn.getInt("data[" + i + "].id");
			System.out.println(id);
			idArr[i] = id;
		}
		Assert.assertEquals(ids[i], idArr[i]);
	}

}
