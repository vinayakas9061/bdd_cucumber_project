package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utility;
import Repository.Request_Body;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.config.RestAssuredConfig;
import io.restassured.config.SSLConfig;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_API_TC1 {

	String requestBody;
	Response response;
	int statuscode;
	String res_name;
	String res_job;
	String res_id;
	String res_createdAt;
	
	@Before
	public void setup() {
		System.out.println("Browser gets started");
	}
	
	@After
	public void teardown() {
		System.out.println("Browser gets closed");
	}

	@Given("Name and job in request body")
	public void name_and_job_in_request_body() throws IOException {
		RestAssured.config = RestAssuredConfig.config().sslConfig(SSLConfig.sslConfig().relaxedHTTPSValidation());

		File dir_Name = Utility.CreateLogDirectory("Post_Api_Log");

		requestBody = Request_Body.Req_Tc1();

		String Endpoint = Request_Body.Hostname() + Request_Body.Resource();

		response = API_Trigger.Post_trigger(Request_Body.Headername(), Request_Body.Headervalue(), requestBody,
				Endpoint);

		Utility.evidenceFileCreator(Utility.testLogName("Post_TestCase_1"), dir_Name, Endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asString());
	}

	@When("send the request with payload to the endpoint")
	public void send_the_request_with_payload_to_the_endpoint() {
		statuscode = response.statusCode();
		ResponseBody res_body = response.getBody();
		res_name = (response.jsonPath().getString("name"));

		res_job = (response.jsonPath().getString("job"));

		res_id = (response.jsonPath().getString("id"));

		res_createdAt = response.jsonPath().getString("createdAt").substring(0, 11);
	}

	@Then("validate status code")
	public void validate_status_code() {
		Assert.assertEquals(statuscode, 201);
	}

	@Then("validate response body parameters")
	public void validate_response_body_parameters() {
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_createdAt, expecteddate);
		Assert.assertNotNull(res_id);

	}

}
